#!/usr/bin/env python

import argparse
import nibabel as nib
import numpy as np
from skimage.filters import threshold_multiotsu
from skimage.measure import label as find_cc
from scipy.ndimage.morphology import binary_fill_holes, distance_transform_edt
from scipy.ndimage.morphology import binary_dilation
from trimesh.voxel.ops import matrix_to_marching_cubes

from improc3d import crop3d


def calc_foreground_mask(image):
    """Calculates foregournd mask using Otsu's threshould.

    Args:
        image (numpy.ndarray): The image to calculate the mask from.

    Returns:
        numpy.ndarray: The foreground mask.

    """
    thresholds = threshold_multiotsu(image)
    fg = image > thresholds[0]
    return fg



def cleanup_fg(fg):
    labels = find_cc(fg)
    counts = np.bincount(labels.flatten())
    for l in np.argsort(counts)[::-1]:
        result = labels == l
        if np.array_equal(np.unique(fg[result]), np.array([True])):
            break
    return result


def fill_holes(fg):
    for axis in [0, 1, 2]:
        tmp = list()
        for image_slice in extract_slices(fg, axis):
            tmp.append(binary_fill_holes(image_slice))
        fg = np.stack(tmp, axis=axis)
    return fg


def extract_slices(image, axis=0):
    if axis == 0:
        for i in range(image.shape[0]):
            yield image[i, :, :]
    elif axis == 1:
        for i in range(image.shape[1]):
            yield image[:, i, :]
    elif axis == 2:
        for i in range(image.shape[2]):
            yield image[:, :, i]


def calc_convex_hull(fg):
    mesh = matrix_to_marching_cubes(fg)
    convex_hull = mesh.convex_hull
    result = convex_hull.voxelized(pitch=1.0).fill().matrix

    starts = -np.ceil(convex_hull.bounds[0]).astype(int)
    stops = np.array(fg.shape) + starts
    bbox = tuple(slice(s, ss) for s, ss in zip(starts, stops))
    result = crop3d(result, bbox, return_bbox=False)
    return result


def fill_bg(image, fg):
    conv_hull = calc_convex_hull(fg).astype(bool)
    bg = np.logical_not(fg)
    bg = binary_dilation(bg, iterations=3)
    dist, indices = distance_transform_edt(bg, return_indices=True)

    bg[np.logical_not(conv_hull)] = False
    for i, (x, y, z) in enumerate(zip(*[ind.flat for ind in indices])):
        if bg.flat[i]:
            image.flat[i] = image[x, y, z]

    return image


def deface(image):
    fg = calc_foreground_mask(image)
    fg = cleanup_fg(fg)
    fg = fill_holes(fg)
    result = fill_bg(image, fg)
    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--image')
    parser.add_argument('-o', '--output')
    args = parser.parse_args()

    obj = nib.load(args.image)
    image = obj.get_fdata(dtype=np.float32)
    result = deface(image)
    out_obj = nib.Nifti1Image(result, obj.affine, obj.header)
    out_obj.to_filename(args.output)
